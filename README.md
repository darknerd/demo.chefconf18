# **ChefConf18**

Ce sont de petits projets qui démontrent de nouvelles innovations découvertes à ChefConf18.

J'ai publié une petite présentation (slidedeck):

  * https://slides.com/devopsstudio/chefconf18#/

## **Les sujets**

* Break It
  * [InSpec GCP Plugin](https://github.com/inspec/inspec-gcp) - Cela permet des tests d'intégration des ressources cloud sur GCP.
  * [Kitchen-Google Plugin](https://github.com/test-kitchen/kitchen-google) - Cela permet de tester des systèmes créés dynamiquement sur GCE.
  * [Kitchen-Kubernetes Plugin](https://github.com/coderanger/kitchen-kubernetes) - Cela permet de tester des systèmes créés dynamiquement sur Kubernetes.
* Build It
  * [Google's Magic Modules](https://github.com/GoogleCloudPlatform/magic-modules) - Ces fonctionnalités vous permettent de générer automatiquement des listes DSL pour Puppet, Chef, Ansible et Terraform qui reflètent Google REST API actuel.
  * [Inspec Iggy Plugin](https://github.com/inspec/inspec-iggy) - Après avoir développé des scripts Terraform, vous pouvez l'utiliser pour générer automatiquement des tests InSpec.
* Change It
  * Améliorations de l'éditeur pour [Atom](https://atom.io/) et [VisualStudio Code](https://code.visualstudio.com/) qui prennent en charge les capacités intégrées de référence et de remplacement automatique
  * [Chef Workstation](https://www.chef.sh/) - Le nom peut ne pas sembler excitant, mais cela ajoute à la fois la simplicité et le déploiement et l'orchestration des capacités, qui ont été absents du chef dans le passé.
  * [Hygieia](https://github.com/capitalone/Hygieia) - Ceci est un tableau de bord qui montre une vue consolidée du pipeline de livraison. Il y a une intégration supportée par Jenkins et SonarQube.

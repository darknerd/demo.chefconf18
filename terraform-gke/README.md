
# **Terraform Google**

## **Generate Credentials**

```bash
DEFAULT_CREDENTIALS=~/.config/gcloud/application_default_credentials.json
export TF_VAR_credentials=~/.config/gcloud/tf_creds.json
gcloud auth application-default login
cp ${DEFAULT_CREDENTIALS} ${TF_VAR_credentials}
```

## **Configure Variables**

Nous devons configurer notre région et notre projet par défaut:

```bash
export TF_VAR_project="$(gcloud config list --format 'value(core.project)')"
export TF_VAR_region="us-east1"
```

## **Initialize Google Provider**

Cela va télécharger les plug-ins nécessaires pour google cloud et configurer notre environnement:

```bash
terraform init
```

## **Create GCE Instances**

```bash
export TF_VAR_user="admin"
export TF_VAR_password="m8XBWrg2zt8R8JoH"
terraform plan  # to look before we leap
terraform apply # create the instances
```

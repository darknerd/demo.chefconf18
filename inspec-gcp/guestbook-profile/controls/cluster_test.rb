# encoding: utf-8
# copyright: 2018, The Authors

title 'GCP Guestbook Cluster'

PROJECT_NAME = attribute('project_name', description: 'gcp project name')
CLUSTER_NAME = attribute('cluster_name', default: 'guestbook', description: 'gcp cluster name')
CLUSTER_ZONE = attribute('cluster_zone', default: 'us-east1-b', description: 'gcp cluster zone')

control 'gcp-1' do
  impact 0.7
  title 'Check Guestbook Cluster'

  describe google_container_cluster(project: PROJECT_NAME, zone: CLUSTER_ZONE, name: CLUSTER_NAME) do
    it { should exist }
    its('name') { should eq CLUSTER_NAME }
    its('status') { should eq 'RUNNING' }
    its('master_auth.username') { should eq 'admin' }
    its('network') { should eq 'default' }
    its('subnetwork') { should eq 'default' }
    its('initial_node_count') { should eq 3 }
  end
end

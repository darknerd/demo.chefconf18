# **Pack de ressources de GCP pour InSpec**

## **Documentation**

Actuellement, il n'y a aucune documentation du pour le projet. Cependant, j'ai trouvé ce blog de l'auteur.

* https://lollyrock.com/articles/inspec-cloud-gcp-setup/

## **Conditions préalables**

### **Téléchargement du logiciel Inspec**

Le logiciel [Inspec](https://www.inspec.io/) 2.x doit être installé: `gem install inspec`.

Par exemple:

```bash
brew install rbenv
eval "$(rbenv init -)"
rbenv install 2.5.1
rbenv shell 2.5.1  # current shell
rbenv global 2.5.1 # all shells
gem install inspec
inspec version # 2.1.84
```

### **Infrastructure**

L'infrastructure google doit déjà avoir été créée. Reportez-vous à Terraform [README.md](../tf/README.md) pour obtenir des instructions.

## **Instructions**

```bash
cat <<-"EOF" > attributes.yml
project_name: $(gcloud config list --format 'value(core.project)')
cluster_region: us-east1
cluster_name: guestbook
EOF

inspec exec guestbook-profile -t gcp:// --attrs attributes.yml
```

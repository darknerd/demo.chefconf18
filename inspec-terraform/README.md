# Iggy

Ceci est une démonstration de l'utilisation d'Iggy, un outil qui peut générer des tests InSpec à partir de votre fichier «Terraform State».

Information:
  * https://github.com/inspec/inspec-iggy
  * https://lollyrock.com/articles/inspec-terraform/

## Téléchargement du logiciel Inspec

J'ai eu des problèmes pour que cela fonctionne avec ChefDK. ChefDK fait d'un softlink une ancienne version du logiciel InSpec. Il est difficile de contourner ce problème.  Pour aggraver le problème, Homebrew Cask n'installe pas non plus la version actuelle de ChefDK.

J'ai dû installer une version de ruby qui est la même que ruby qui vient avec ChefDK pour obtenir le dernier logiciel InSpec.

```bash
brew install rbenv
eval "$(rbenv init -)"
rbenv install 2.5.1
rbenv shell 2.5.1  # current shell
rbenv global 2.5.1 # all shells
gem install inspec
inspec version # 2.1.84
gem install inspec-iggy
inspec terraform version # Iggy v0.2.0
```

## Utilisation

```bash
inspec terraform generate --tfstate ../tf/terraform.tfstate
```

## Limites

* https://github.com/inspec/inspec-iggy/issues/6
